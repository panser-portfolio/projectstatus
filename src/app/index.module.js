(function() {
  'use strict';

  angular
    .module('angular', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'restangular',
      'ui.router',
      'ui.bootstrap',
      'ui.bootstrap.datetimepicker',
      'toastr'
    ]);

})();
