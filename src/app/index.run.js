(function() {
  'use strict';

  angular
    .module('angular')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope) {

    var deregisterationCallback = $rootScope.$on('$stateChangeError', function(){
      throw arguments[5];
    });
    $rootScope.$on('$destroy', deregisterationCallback)

    $log.debug('runBlock end');
  }

})();
