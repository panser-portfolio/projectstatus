/*eslint no-unused-vars: [2, {"vars": "all", "args": "none"}]*/

(function () {
  'use strict';

  angular
    .module('angular')
    .controller('ProjectDeleteController', ProjectDeleteController);

  function ProjectDeleteController($uibModalInstance, entity) {
    var vm = this;

    vm.project = entity;
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(id) {
      vm.project.remove().then(onSaveSuccess);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(true);
    }

  }
})();
