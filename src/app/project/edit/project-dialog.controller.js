(function () {
  'use strict';

  angular
    .module('angular')
    .controller('ProjectDialogController', ProjectDialogController);

  function ProjectDialogController($timeout, $uibModalInstance, AppApi, entity) {
    var vm = this;

    vm.project = entity;
    vm.clear = clear;
    vm.datePickerOpenStatus = {};
    vm.openCalendar = openCalendar;
    vm.save = save;

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.project.id !== null) {
        var editProject = AppApi.copy(vm.project);
        editProject.save().then(onSaveSuccess, onSaveError);
      } else {
        AppApi.restangularizeElement(null, vm.project, 'project').post().then(onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    vm.datePickerOpenStatus.createDate = false;
    vm.datePickerOpenStatus.completeDate = false;

    function openCalendar(date) {
      vm.datePickerOpenStatus[date] = true;
    }
  }
})();
