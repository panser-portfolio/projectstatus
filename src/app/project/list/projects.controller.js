(function() {
  'use strict';

  angular
    .module('angular')
    .controller('ProjectController', ProjectController);

  /** @ngInject */
  function ProjectController(AppApi) {
    var vm = this;

    vm.projects = AppApi.all('project').getList().$object;
  }
})();
