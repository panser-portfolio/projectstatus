(function () {
  'use strict';

  angular
    .module('angular')
    .controller('ProjectDetailController', ProjectDetailController);

  function ProjectDetailController(entity) {
    var vm = this;

    vm.project = entity;
  }
})();
