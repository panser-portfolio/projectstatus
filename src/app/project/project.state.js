(function () {
  'use strict';

  angular
    .module('angular')
    .config(stateConfig);

  /** @ngInject */
  function stateConfig($stateProvider) {

    $stateProvider
      .state('project', {
        //parent: 'app',
        url: '/project',
        //views: {
        //  'content@': {
        //    templateUrl: 'app/project/projects.html',
        //    controller: 'ProjectController',
        //    controllerAs: 'vm'
        //  }
        //},
        templateUrl: 'app/project/list/projects.html',
        controller: 'ProjectController',
        controllerAs: 'vm'
      })
      .state('project-detail', {
        //parent: 'project',
        url: '/project/{id}',
        templateUrl: 'app/project/detail/project-detail.html',
        controller: 'ProjectDetailController',
        controllerAs: 'vm',
        resolve: {
          entity: ['$stateParams', 'AppApi', function($stateParams, AppApi) {
            return AppApi.one('project', $stateParams.id).get().$object;
          }]
        }
      })
      .state('project.new', {
        //parent: 'project',
        url: '/project/new',
        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/project/edit/project-dialog.html',
            controller: 'ProjectDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  createDate: null,
                  completeDate: null,
                  manager: null,
                  riskRating: null,
                  cpi: null,
                  spi: null,
                  id: null
                };
              }
            }
          }).result.then(function() {
            $state.go('project', null, { reload: true });
          }, function() {
            $state.go('project');
          });
        }]
      })
      .state('project.edit', {
        //parent: 'project',
        url: '/project/{id}/edit',
        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/project/edit/project-dialog.html',
            controller: 'ProjectDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['AppApi', function(AppApi) {
                return AppApi.one('project', $stateParams.id).get().$object;
              }]
            }
          }).result.then(function() {
            $state.go('project', null, { reload: true });
          }, function() {
            $state.go('^');
          });
        }]
      })
      .state('project.delete', {
        //parent: 'project',
        url: '/{id}/delete',
        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/project/delete/project-delete-dialog.html',
            controller: 'ProjectDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['AppApi', function(AppApi) {
                return AppApi.one('project', $stateParams.id).get().$object;
              }]
            }
          }).result.then(function() {
            $state.go('project', null, { reload: true });
          }, function() {
            $state.go('^');
          });
        }]
      })
    ;

  }

})();
