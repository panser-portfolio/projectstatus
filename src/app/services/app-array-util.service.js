/**
 * Created by spanov on 6/21/2016.
 */
(function () {
  'use strict';

  angular
    .module('angular')
    .factory('AppArrayUtils', AppArrayUtils);

  function AppArrayUtils() {
    var service = {
      findById: findById
    };

    return service;

    function findById(array, id) {
      var element = array.filter(function(elm) {
        return elm.id == id;
      })[0];

      return element;
    }

  }
})();
