/**
 * Created by spanov on 6/21/2016.
 */
(function () {
  'use strict';

  angular
    .module('angular')
    .factory('AppApi', AppApi);

  function AppApi(Restangular, $log, $q, AppDateUtils) {
    var service = Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer
        .setBaseUrl('http://localhost:3004/')
        .setErrorInterceptor(function (response) {
          $log.log('REST REQ ERROR(' + response.config.method + ' ' + response.config.url + '): ' + response.status + ' ' + response.statusText);
          return true; // futher processing this error
        })
        .addElementTransformer('project', false, function (element) {
          element.createDate = AppDateUtils.convertDateTimeFromServer(element.createDate);
          element.completeDate = AppDateUtils.convertDateTimeFromServer(element.completeDate);
          return element;
        })
      ;
    });

    return service;
  }
})();
