/**
 * Created by spanov on 6/21/2016.
 */
'use strict';

var gulp = require('gulp');
var jsonServer = require('gulp-json-srv');

gulp.task('json-server', function () {
  jsonServer.start({
    data: 'json-server-data.json',
    port: 3004
  });
});
